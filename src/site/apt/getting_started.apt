Getting started

* Quick start
 
 The interfaces defined by the application are called <<Property Interfaces>>. Here is an example of a Property Interface:
 
+---+
public interface ServerProp extends PropertyBase {
	@PropertyGetter (name = "server.name", defaultValue = "localhost")
	public String getServerName();
}
+---+

 As can be seen, the Property Interface:
 
  * Must extend the {{{./apidocs/net/toften/prop4j/PropertyBase.html}PropertyBase}} interface
 
  * A property getter method must have the {{{./apidocs/net/toften/prop4j/PropertyGetter.html}PropertyGetter}} annotation
 
 To access the property values defined in the Property Interface do the following
 
+---+
 ...
 include net.toften.prop4j.Prop;
 ...
 	ServerProp serverInfo = Prop.getProp(ServerProp.class);
 ...
 	Socket s = new Socket(serverInfo.getServerName());
 ...
+---+

* More detail

** Create Property Interface

 The first thing to do, is to create a Property Interface to define the properties we want prop4j to manage. To start with, we will ignore how the value of these properties are persisted.
 A simple Property Interface:

+---+
public interface ConnectionProperties extends PropertyBase {
	@PropertyGetter ( name = "host" )
	public String getHostname();
	
	@PropertyGetter ( name = "port", defaultValue = 8000 )
	public Integer getPort();
}
+---+

 To start using the Property Interface, we need to get an instance of it. This is done like this:

+---+
	...
	ConnectionProperties cp = Prop.getProp(ConnectionProperties.class);
	...
	String host = cp.getHostname();
	int port = cp.getPort();
	...
+---+

 In the example above, the <<<getHostname()>>> method will return an empty string, and the <<<getPort()>>> method will return 8000.

*** Adding setters

 To be able to manipulate the property values, we will need to add setters. Let's adapt the interface defined previously:

+---+
public interface ConnectionProperties extends PropertyBase {
	@PropertyGetter ( name = "host" )
	public String getHostname();
	
	@PropertyGetter ( name = "port", defaultValue = 8000 )
	public Integer getPort();
	
	@PropertySetter ( name = "host" )
	public void setHostname(String host);
}
+---+

 We have now added a setter method, so lets try this:

+---+
	...
	ConnectionProperties cp = Prop.getProp(ConnectionProperties.class);
	...
	String host = cp.getHostname(); // Returns an empty string
	...
	cp.setHostname("localhost");
	String host = cp.getHostname(); // Returns "hostname"
	...
+---+

 No major surprised there, and this is really all there is to using Property Interfaces.

** Property Loaders

 So where does the property values come from? In the example above, the properties are managed by the default {{{./apidocs/net/toften/prop4j/loaders/PropertyLoader.html}PropertyLoader}}. This Property Loader will store the values in memory, and any changes made using the property setters will be lost when the application terminates.


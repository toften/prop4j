package net.toften.prop4j;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import net.toften.prop4j.loaders.PropertyLoader;


/**
 * This annotation is used to define a getter method for a property in a Property Interface.
 * 
 * @author thomaslarsen
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyGetter {
    /**
     * This is the name or key of the property.
     * <p>
     * This is used by the {@link PropertyLoader} to identify the value that should be returned by the
     * {@link PropertyLoader#getProperty(String)} method where the name is passed in as the parameter.
     * <p>
     * The name is a String and can contain any characters.
     */
    String name();

    /**
     * The value returned if the {@link PropertyLoader} cannot find a value.
     * <p>
     * The default value of a property defaults to an empty String, or whatever value an empty String is converted to, to adapt to
     * the return value defined by the method in the Property Interface.
     */
    String defaultValue() default "";
}
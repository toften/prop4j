package net.toften.prop4j;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import net.toften.prop4j.loaders.PropertyLoader;


/**
 * This annotation is used to define a setter method for a property in a Property Interface.
 * 
 * @author thomaslarsen
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertySetter {
    /**
     * This is the name or key of the property.
     * <p>
     * This is used by the {@link PropertyLoader} to identify the value that should set by the
     * {@link PropertyLoader#setProperty(String, Object)} method where the name is passed in as the parameter.
     * <p>
     * The name is a String and can contain any characters.
     */
    String name();
}
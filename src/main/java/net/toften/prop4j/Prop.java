package net.toften.prop4j;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import net.toften.prop4j.config.ManualConfig;
import net.toften.prop4j.config.Prop4J;
import net.toften.prop4j.config.Prop4jConfig;
import net.toften.prop4j.config.XMLConfig;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.loaders.SystemPropertiesLoader;

/**
 * The methods in this class provides the facility to instantiate Property Interfaces.
 * <p>
 * This class can not be instantiated. All methods provided by this class are static.
 * <p>
 * To get an instance of a property interface use the {@link #getProp(Class)} method. This method should really be the only one
 * that is needed!
 * <h1>Configuration</h1> Prop4j can be configured either using a properties file or programatically.
 * <h2>Prop4j properties file</h2> The properties file is loosely bases on the structure of the {@link http://log4j.org log4j} XML
 * properties file. The {@link Prop4jProperties#getConfigurationURL()} method (and underlying
 * {@link Prop4jProperties#PROP_PROPERTIES} property controls the actual configuration of prop4j.
 * <h2>Programmatically</h2> Use the {@link ManualConfig} class.
 * prop4j can be configured using the methods in the {@link Prop4jConfig} interface. The key aspects of
 * configuring prop4j is to add {@link PropertyLoader}s used to load the actual property values.
 * 
 * @author thomaslarsen
 */
@SuppressWarnings("unchecked")
public final class Prop {
    private static Prop4jConfig config;

    private static Prop4jProperties propProperties;

    /**
     * This Map will cache all created Property Interfaces.
     */
    private static Map<Class<? extends PropertyBase>, PropertyBase> propHandlerCache;

    static {
        // Implicitly invoke the init method
        init();
    }

    /**
     * Initialise prop4j.
     * <p>
     * This method will initialise prop4j. When prop4j is initialised, the following occurs:
     * <ul>
     * <li>The {@link #getProp4jProperties() properties} are re-initialised</li>
     * <li>The Property Interface cache is cleared</li>
     * <li>The {@link #getProp4jConfig() configuration} is reloaded</li>
     * </ul>
     * This method is implicitly invoked by a <code>static</code> block, when this class is loaded.
     * <h3>Re-initialisation</h3> prop4j can be re-initialised at any time.
     */
    public static void init() {
        initPropertyCache();
        try {
            initProperties();
            
            if (!getProp4jProperties().getConfigurationURL().equals("")) {
                final String confURL = getProp4jProperties().getConfigurationURL();
                System.out.println("Loading configuration from URL: " + confURL);

                final JAXBContext c = JAXBContext.newInstance("net.toften.prop4j.config");
                final Unmarshaller um = c.createUnmarshaller();

                final URL confXML = new URL(confURL);
                final Prop4J prop4jConfig = (Prop4J) um.unmarshal(confXML);
                config = new XMLConfig(prop4jConfig);
            } else {
                final String defaultConfigClass = getProp4jProperties().getDefaultConfigClassName();
                System.out.println("Instantiating configuration class: " + defaultConfigClass);

                final Class<? extends Prop4jConfig> configClass =
                    (Class<? extends Prop4jConfig>) Class.forName(defaultConfigClass);
                config = configClass.newInstance();
            }
        } catch (final Exception e) {
            e.printStackTrace(System.err);
        }
    }
    
    public static void init(final Prop4jConfig newConfig) {
        initPropertyCache();
        try {
            initProperties();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        
        config = newConfig;
        System.out.println("Manually setting configuration with class: " + config.getClass().getName());
    }

    private static void initProperties() throws BadPropertyInfoException {
        /*
         * Eat our own dogfood, and wrap our property interface in the System Properties
         */
        propProperties =
            new PropertyHandler<Prop4jProperties>(Prop4jProperties.class, new SystemPropertiesLoader()).getProxy();

        // Add the porp4j properties to the cache
        propHandlerCache.put(Prop4jProperties.class, propProperties);
    }

    private static void initPropertyCache() {
        propHandlerCache = new HashMap<Class<? extends PropertyBase>, PropertyBase>();
    }

    /**
     * Private constructor - to prevent instantiation.
     */
    private Prop() {
        // Empty
    }

    /**
     * Get the properties for prop4j.
     * 
     * @return prop4j properties
     */
    public static Prop4jProperties getProp4jProperties() {
        if (propProperties == null) {
            throw new IllegalStateException("Prop4jProperties has not been initialised");
        }

        return propProperties;
    }

    /**
     * Return an instance of a PropertyInterface.
     * <p>
     * 
     * @param <T>
     *            The PropertyInterface type
     * @param propertyInterface
     *            The class of the PropertyInterface
     * @return an instance of the PropertyInterface
     * @throws BadPropertyInfoException
     *             if the PropertyInterface is badly configured
     */
    public static <T extends PropertyBase> T getProp(final Class<T> propertyInterface) throws BadPropertyInfoException {
        T propertyHandler;

        // First check the cache
        if (propHandlerCache.containsKey(propertyInterface)) {
            propertyHandler = (T) propHandlerCache.get(propertyInterface);
        } else {
            // ...not there...OK create it and put it in the cache
            propertyHandler = new PropertyHandler<T>(propertyInterface).getProxy();
            propHandlerCache.put(propertyInterface, propertyHandler);
        }

        return propertyHandler;
    }

    /**
     * Get the configuration for prop4j.
     * 
     * @return prop4j configuration
     */
    public static Prop4jConfig getProp4jConfig() {
        if (config == null) {
            throw new IllegalStateException("Prop4jConfig has not been initialised");
        }

        return config;
    }
}
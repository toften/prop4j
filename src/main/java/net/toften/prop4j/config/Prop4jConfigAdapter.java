package net.toften.prop4j.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.prop4j.Prop;
import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.loaders.PropertyLoaderInitException;

/**
 * This adapter class should be the base for all prop {@link Prop4jConfig configuration} implementations.
 * <p>
 * It provides support for managing a hierarchy of <code>PropertyLoader</code>s organised by the
 * path they were added with.
 * 
 * @author thomaslarsen
 */
public abstract class Prop4jConfigAdapter
    implements
        Prop4jConfig {
    /**
     * This class represents a {@link PropertyLoader} in the loader hierarchy.
     * <p>
     * It will order the loaders in a hierarchy, where each element in the hierarchy can be associated with a PropertyLoader.
     * <p>
     * The {@link Prop4jConfig#getDefaultLoader() default loader} can be added by calling
     * {@link Prop4jConfig#addLoader(PropertyLoader, String)} with a <code>null</code> path.
     * 
     * @author thomaslarsen
     * @see Prop4jConfig#getLoader(Class)
     */
    protected static class LoaderDescriptor {
        protected PropertyLoader loader;

        /**
         * Child loaders for this element.
         */
        private final Map<String, LoaderDescriptor> children = new HashMap<String, LoaderDescriptor>();

        private final LoaderDescriptor parent;

        /**
         * Create a new loader descriptor.
         * 
         * @param parent the parent of the new loader; or <code>null</code> if it is the <code>rootLoader</code>.
         */
        private LoaderDescriptor(final LoaderDescriptor parent) {
            this.parent = parent;
        }

        /**
         * This method will return the PropertyLoader represented by this LoaderDescriptor if it exists. If it doesn't exist it
         * will go up the tree until it finds a LoaderDescriptor with a PropertyLoader, or return <code>null</code> if it gets to
         * the top of the tree and finds no PropertyLoader there.
         * 
         * @return a valid PropertyLoader, or <code>null</code>
         */
        public PropertyLoader getLastPresentLoader() {
            if (this.loader != null) {
                return this.loader;
            } else if (parent != null) {
                return parent.getLastPresentLoader();
            } else {
                return null;
            }
        }

        private LoaderDescriptor addChildLoader(final String pathElement) {
            final LoaderDescriptor newElement = new LoaderDescriptor(this);

            children.put(pathElement, newElement);

            return newElement;
        }
    }

    /**
     * The root of the loader hierarchy.
     */
    protected final LoaderDescriptor rootElement = new LoaderDescriptor(null);

    @SuppressWarnings("unchecked")
    @Override
    public PropertyLoader addLoader(final String loaderClassName, final Properties loaderProperties, final String path)
        throws PropertyLoaderInitException {
        if (loaderClassName == null)
            throw new NullPointerException("Provided loader class name is null");
        
        // Instantiate and initialise the new loader
        PropertyLoader newLoader = null;

        try {
            final Class<? extends PropertyLoader> loaderClass = (Class<? extends PropertyLoader>) Class.forName(loaderClassName);

            newLoader = loaderClass.newInstance();
            if (loaderProperties != null)
                newLoader.init(loaderProperties);
        } catch (final Exception ex) {
            throw new PropertyLoaderInitException(ex);
        }

        /*
         * Use the "other" add method to add the loader to the hierarchy
         */
        return addLoader(newLoader, path);
    }

    @Override
    public PropertyLoader addLoader(final PropertyLoader loader, final String path) {
        LoaderDescriptor currentElement = rootElement;

        // Analyse the path
        if (path != null) {
            final String[] pathElements = path.split(Prop.getProp4jProperties().getPathSplitRegex());

            // Create the path of LoaderElements (if necessary)
            for (int i = 0; i < pathElements.length; i++) {
                if (currentElement.children.containsKey(pathElements[i])) {
                    currentElement = currentElement.children.get(pathElements[i]);
                } else {
                    currentElement = currentElement.addChildLoader(pathElements[i]);
                }
            }
        }

        currentElement.loader = loader;

        return currentElement.loader;
    }

    @Override
    public PropertyLoader getDefaultLoader() {
        return rootElement.getLastPresentLoader();
    }

    @Override
    public PropertyLoader getLoader(final Class<? extends PropertyBase> propertyInterface) {
        return getLoader(propertyInterface.getName());
    }
    
    @Override
    public PropertyLoader getLoader(String path) {
        final String[] pathElements = path.split(Prop.getProp4jProperties().getPathSplitRegex());

        LoaderDescriptor currentElement = rootElement;

        for (int i = 0; i < pathElements.length; i++) {
            if (currentElement.children.containsKey(pathElements[i])) {
                currentElement = currentElement.children.get(pathElements[i]);
            } else {
                break;
            }
        }

        return currentElement.getLastPresentLoader();
    }

    /**
     * @return the number of {@link PropertyLoader}s in total.
     */
    public int getLoaderCount() {
        return getLoaderCount(rootElement);
    }

    /**
     * @param element the element to count the loaders from
     * @return the number of {@link PropertyLoader}s from a given element
     */
    protected static int getLoaderCount(final LoaderDescriptor element) {
        int count = element.loader == null ? 0 : 1;

        for (final LoaderDescriptor e : element.children.values()) {
            count += getLoaderCount(e);
        }

        return count;
    }
}
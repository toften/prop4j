package net.toften.prop4j.config;

import java.util.Properties;

import net.toften.prop4j.Prop;
import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.loaders.PropertyLoaderInitException;

/**
 * The configuration classes are responsible for providing {@link PropertyLoader}s for <code>Property Interface</code>s.
 * <p>
 * A <code>PropertyLoader</code> is found by looking at the package of the PropertyInterface, which is called it's
 * <i>path</i>.
 * <p>
 * When a <code>PropertyLoader</code> is added, a path is specified. This path is used to look up
 * the correct <code>PropertyLoader</code> when a <code>Property Interface</code> is requested using
 * the {@link Prop#getProp(Class)} method.
 * <h3>Default loader</h3>
 * A {@link #getDefaultLoader() default} <code>PropertyLoader</code> must <i>always</i> be added when
 * the implementing class is instantiated.
 * <p>
 * A default loader is added by passing in a <code>null</code> value as the path to either of the
 * add methods: {@link #addLoader(PropertyLoader, String)} or {@link #addLoader(String, Properties, String)}
 * <br>
 * The default loader can be replaced at any time.
 * 
 * <h3>Adding a <code>PropertyLoader</code></h3>
 * When adding a <code>PropertyLoader</code>, a <i>path</i> must be supplied.
 * This path specifies the which <code>Property Interface</code>s the loader will supply
 * values for.<br>
 * When a <code>PropertyLoader</code> is requested with the {@link #getLoader(Class)} method, the
 * <i>fully qualified name (FQN)</i> of the <code>Property Interface</code> is used to match up
 * with a path for a <code>PropertyLoader</code>. For example:
 * <p>
 * If a single <code>PropertyLoader</code> <i>ConfigLoader</i> is supplied with a path of:
 * <pre>com.example.app.config</pre>
 * the following <code>Property Interface</code>S will load with:
 * <p>
 * <table><tbody>
 * <tr><th>Property Interface FQN</th><th>PropertyLoader</th></tr>
 * <tr><td>com.example.MainConfig</td><td>DefaultLoader</td>
 * <tr><td>com.example.app.user.UserProperties</td><td>DefaultLoader</td>
 * <tr><td>com.example.app.user.MyProperties</td><td>DefaultLoader</td>
 * <tr><td>com.example.app.config.AppConfig</td><td>ConfigLoader</td>
 * <tr><td>com.example.app.config.plugin.PluginProperties</td><td>ConfigLoader</td>
 * </tbody></table>
 * <p>
 * If we add another <code>PropertyLoader</code> <i>UserLoader</i> with the path:
 * <pre>com.example.app.user</pre>
 * then the <code>Property Interface</code>s will now load like this:
 * <p>
 * <table><tbody>
 * <tr><th>Property Interface FQN</th><th>PropertyLoader</th></tr>
 * <tr><td>com.example.MainConfig</td><td>DefaultLoader</td>
 * <tr><td>com.example.app.user.UserProperties</td><td><strong>UserLoader</strong></td>
 * <tr><td>com.example.app.user.MyProperties</td><td><strong>UserLoader</strong></td>
 * <tr><td>com.example.app.config.AppConfig</td><td>ConfigLoader</td>
 * <tr><td>com.example.app.config.plugin.PluginProperties</td><td>ConfigLoader</td>
 * </tbody></table>
 * <p>
 * Finally, if we add a third <code>PropertyLoader</code> <i>SpecialUserLoader</i> with the path:
 * <pre>com.example.app.user.UserProperties</pre>
 * then we get this result
 * <p>
 * <table><tbody>
 * <tr><th>Property Interface FQN</th><th>PropertyLoader</th></tr>
 * <tr><td>com.example.MainConfig</td><td>DefaultLoader</td>
 * <tr><td>com.example.app.user.UserProperties</td><td><strong>SpecialUserLoader</strong></td>
 * <tr><td>com.example.app.user.MyProperties</td><td>UserLoader</td>
 * <tr><td>com.example.app.config.AppConfig</td><td>ConfigLoader</td>
 * <tr><td>com.example.app.config.plugin.PluginProperties</td><td>ConfigLoader</td>
 * </tbody></table>
 * 
 * @author thomaslarsen
 */
public interface Prop4jConfig {
    /**
     * Get a {@link PropertyLoader} responsible for providing the property values for a Property Interface.
     * <p>
     * 
     * 
     * @param propertyInterface
     *            The <code>Property Interface</code> to provide a PropertyLoader for
     * @return the <code>PropertyLoader</code>
     */
    PropertyLoader getLoader(Class<? extends PropertyBase> propertyInterface);

    /**
     * Return the {@link PropertyLoader} for a given path.
     * 
     * @param path the <code>ProperyLoader</code> to get
     * 
     * @return the <code>PropertyLoader</code>
     */
    PropertyLoader getLoader(String path);
    
    /**
     * This method returns the PropertyLoader used if no added loaders can be found for a given Property Interface.
     * 
     * @return the default PropertyLoader
     */
    PropertyLoader getDefaultLoader();

    /**
     * Add a new {@link PropertyLoader} to the configuration.
     * <p>
     * This method will instantiate, {@link PropertyLoader#init(Properties) initialise} and add a <code>PropertyLoader</code>. The
     * method is responsible for calling the {@link PropertyLoader#init(Properties)} method with the <code>loaderProperties</code>
     * passed into this method. If the <code>loaderProperties</code> are <code>null</code>, then the <code>init</code> method
     * will <strong>not</strong> be invoked.
     * 
     * @param loaderClassName
     *            The class name of the {@link PropertyLoader}
     * @param loaderProperties
     *            The properties to pass to the <code>PropertyLoader</code>s {@link PropertyLoader#init(Properties)} method
     * @param path
     *            The package path of the PropertyInterface this loader will provide values for
     * @return a new <code>PropertyLoader</code>
     * @throws PropertyLoaderInitException
     * @see {@link #addLoader(PropertyLoader, String)}
     */
    PropertyLoader addLoader(String loaderClassName, Properties loaderProperties, String path);

    /**
     * Add an existing {@link PropertyLoader} to the configuration.
     *<p>
     * This method will add an already initialised PropertyLoader to the configuration.
     * <p>
     * Note, that if the <code>path</code> is <code>null</code>, then the {@link #getDefaultLoader() default}
     * 
     * @param loader
     *            the PropertyLoader to add
     * @param path
     *            The package path of the PropertyInterface this loader will provide values for
     * @return the added PropertyLoader. This is the same object passed in as the <code>loader</code>
     */
    PropertyLoader addLoader(PropertyLoader loader, String path);
}
package net.toften.prop4j.config;

import java.util.Properties;

import net.toften.prop4j.BadPropertyInfoException;

public class XMLConfig
    extends
        Prop4jConfigAdapter
    implements
        Prop4jConfig {
    public XMLConfig(final Prop4J config)
        throws BadPropertyInfoException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        for (final Loader w : config.getLoaders().getLoader()) {
            final Properties loaderProperties = new Properties();
            if (w.getParameters() != null && w.getParameters().getParameter() != null) {
                for (final Property p : w.getParameters().getParameter()) {
                    loaderProperties.put(p.getKey(), p.getValue());
                }
            }

            addLoader(w.getClazz(), loaderProperties, w.getPath());
        }
    }
}
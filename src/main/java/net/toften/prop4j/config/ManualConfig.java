package net.toften.prop4j.config;

import net.toften.prop4j.loaders.MemoryPropertyLoader;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.loaders.PropertyLoaderInitException;

/**
 * This implementation of {@link Prop4jConfig} allows the application
 * to configure prop4j programatically.
 * <p>
 * A {@link Prop4jConfig#getDefaultLoader() default} {@link PropertyLoader} will be
 * added. This will be implemented by the {@link MemoryPropertyLoader}.
 * 
 * @author thomaslarsen
 */
public class ManualConfig
    extends
        Prop4jConfigAdapter
    implements
        Prop4jConfig {

    /**
     * Instantiates the {@link Prop4jConfig} implementation.
     * <p>
     * An instance of the {@link MemoryPropertyLoader} is added as the default loader.
     * 
     * @throws PropertyLoaderInitException
     */
    public ManualConfig()
        throws PropertyLoaderInitException {
        // Add the default loader
        addLoader(MemoryPropertyLoader.class.getName(), null, null);
    }
}
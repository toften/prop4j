package net.toften.prop4j.loaders;

import java.util.Properties;

import net.toften.prop4j.BadPropertyInfoException;

/**
 * Provides a <i>memory only</i> {@link PropertyLoader} implementation.
 * <p>
 * It will hold property values in memory, and it will be {@link #isMutable() mutable}.
 * 
 * @author thomaslarsen
 */
public class MemoryPropertyLoader
    implements
        PropertyLoader {
    private final Properties properties;

    public MemoryPropertyLoader() {
        properties = new Properties();
    }

    /**
     * Initialise with a set of properties.
     * 
     * @param properties
     */
    public MemoryPropertyLoader(final Properties properties) {
        this.properties = properties;
    }

    @Override
    public void setProperty(final String propName, final Object value) {
        properties.put(propName, value);
    }

    @Override
    public void init(final Properties adapterProperties)
        throws BadPropertyInfoException {
        // Empty
    }

    @Override
    public Object getProperty(final String name) {
        return properties.get(name);
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    public boolean isSingleton() {
        return false;
    }
}
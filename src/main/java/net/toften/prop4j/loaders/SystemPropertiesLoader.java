package net.toften.prop4j.loaders;

import java.util.Properties;

/**
 * Wraps the system properties as a {@link PropertyLoader}.
 * 
 * @author thomaslarsen
 */
public final class SystemPropertiesLoader
    implements
        PropertyLoader {
    @Override
    public Object getProperty(final String name) {
        return System.getProperties().getProperty(name);
    }

    @Override
    public void init(final Properties loaderProperties) {
        // Empty
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public void setProperty(final String name, final Object value) {
        System.getProperties().setProperty(name, value.toString());
    }
}
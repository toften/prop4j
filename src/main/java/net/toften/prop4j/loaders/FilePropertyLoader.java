package net.toften.prop4j.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.PropertyGetter;


public class FilePropertyLoader
    extends
        PropertyLoaderAdapter<FilePropertyLoader.LoaderPropertyInterface>
    implements
        PropertyLoader {
    public interface LoaderPropertyInterface
        extends
            PropertyBase {
        public static final String FILENAME_PROPERTY = "filename";

        @PropertyGetter(name = FILENAME_PROPERTY)
        String getFileName();
    }

    final Properties prop = new Properties();

    @Override
    public void init(final Properties loaderProperties) throws BadPropertyInfoException {
        super.init(loaderProperties);

        final File propFile = new File(getLoaderProperties().getFileName());
        InputStream propStream = null;
        try {
            propStream = new FileInputStream(propFile);
            prop.load(propStream);
        } catch (final Exception e) {
            throw new PropertyLoaderInitException(e);
        } finally {
            if (propStream != null) {
                try {
                    propStream.close();
                } catch (final IOException e) {
                    throw new PropertyLoaderInitException(e);
                }
            }
        }
    }

    @Override
    protected Class<LoaderPropertyInterface> getLoaderPropertyInterface() {
        return FilePropertyLoader.LoaderPropertyInterface.class;
    }

    @Override
    public Object getProperty(final String name) {
        return prop.get(name);
    }
}
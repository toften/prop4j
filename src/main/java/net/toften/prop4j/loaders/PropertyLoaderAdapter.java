package net.toften.prop4j.loaders;

import java.util.Properties;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.PropertyHandler;
import net.toften.prop4j.config.Prop4jConfig;

/**
 * This class provides an adapter for implementations of the {@link PropertyLoader} interface.
 * <p>
 * The adapter supports the definition of a <code>Property Interface</code> for the extending class.
 * The values used in this interface is taken from the {@link Properties} passed into the {@link #init(Properties)} method.
 * 
 * @author thomaslarsen
 * @param <T> the <code>Property Interface</code> of the loader properties
 * @see Prop4jConfig
 */
public abstract class PropertyLoaderAdapter<T extends PropertyBase>
    implements
        PropertyLoader {
    /**
     * The LocalLoader is used to wrap the loader properties and provide access
     * to them using a <code>Property Interface</code>.
     * <p>
     * We <i>always</i> eat our own dogfood!
     * 
     * @author thomaslarsen
     */
    private class LocalLoader
        implements
            PropertyLoader {

        private final Properties properties;

        public LocalLoader(final Properties properties) {
            this.properties = properties;
        }

        @Override
        public Object getProperty(final String name) {
            return properties.get(name);
        }

        @Override
        public void init(final Properties loaderProperties)
            throws BadPropertyInfoException, PropertyLoaderInitException {
            // Empty
        }

        @Override
        public boolean isMutable() {
            return false;
        }

        public boolean isSingleton() {
            return false;
        }

        @Override
        public void setProperty(final String name, final Object value)
            throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }
    }

    private T adapterProperties;

    @Override
    public void init(final Properties loaderProperties) throws BadPropertyInfoException {
        adapterProperties = new PropertyHandler<T>(getLoaderPropertyInterface(), new LocalLoader(loaderProperties)).getProxy();
    }

    /**
     * @return
     */
    protected abstract Class<T> getLoaderPropertyInterface();

    /**
     * @return the loader properties
     * 
     * @see #getLoaderPropertyInterface()
     */
    protected T getLoaderProperties() {
        return adapterProperties;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    public boolean isSingleton() {
        return false;
    }

    @Override
    public void setProperty(final String prop, final Object value) {
        throw new UnsupportedOperationException();
    }
}
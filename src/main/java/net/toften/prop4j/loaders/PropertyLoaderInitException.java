package net.toften.prop4j.loaders;

public class PropertyLoaderInitException
    extends
        RuntimeException {

    public PropertyLoaderInitException(final Exception e) {
        super(e);
    }
}

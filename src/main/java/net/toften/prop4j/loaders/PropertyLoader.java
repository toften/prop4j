package net.toften.prop4j.loaders;

import java.util.Properties;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.config.Prop4jConfig;


/**
 * The PropertyLoader is responsible for getting and setting the values of the <code>Property Interface</code>s.
 * <p>
 * The PropertyLoader class is declared in the {@link Prop4jConfig}, and different loaders can 
 * be provided for different <code>Property Interface</code>s.
 * <p>
 * PropertyLoader implementations must implement <i>only</i> the default constructor.
 * 
 * @author thomaslarsen
 * @see PropertyLoaderAdapter
 */
public interface PropertyLoader {
    /**
     * This method will be called by the {@link Prop4jConfig} implementation responsible for creating the ProperyLoader.
     * 
     * @param loaderProperties
     *            the loader properties taken from the {@link Prop4jConfig}
     * @throws BadPropertyInfoException
     *             if the information about the specified properties are incorrect
     * @throws PropertyLoaderInitException
     *             if the initialisation fails
     */
    void init(final Properties loaderProperties) throws BadPropertyInfoException, PropertyLoaderInitException;

    /**
     * Sets the property value.
     * 
     * @param name
     *            the property name
     * @param value
     *            the value to assign to the property
     * @throws UnsupportedOperationException
     *             if the PropertyLoader is not {@link #isMutable() mutable}
     * @see PropertyBase#setProperty(String, Object)
     */
    void setProperty(final String name, Object value) throws UnsupportedOperationException;

    /**
     * Get the value of a named property.
     * 
     * @param name
     *            the property name
     * @return the value of the property
     * @see PropertyBase#getProperty(String)
     */
    Object getProperty(final String name);

    /**
     * @return <code>true</code> if the PropertyLoader properties are mutable
     */
    boolean isMutable();
}
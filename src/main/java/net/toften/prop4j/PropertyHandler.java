package net.toften.prop4j;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import net.toften.prop4j.loaders.PropertyLoader;

/**
 * @author thomaslarsen
 * @param <T> the <code>Property Interface</code>
 */
public class PropertyHandler<T extends PropertyBase>
    implements
        InvocationHandler,
        PropertyBase {

    private static class PropertyInfo {
        private PropertyGetter getter;
        private PropertySetter setter;

        public void setGetter(final PropertyGetter pi) {
            this.getter = pi;
        }

        public void setSetter(final PropertySetter ps) {
            this.setter = ps;
        }

        public boolean hasGetter() {
            return getter != null;
        }

        public boolean hasSetter() {
            return setter != null;
        }
    }

    private final Class<T> propertyInterface;

    private PropertyChangeSupport pSupport;

    private T proxy;

    /**
     * The property loader for the property interface.
     * <p>
     * Can be <code>null</code>
     */
    private PropertyLoader loader;

    /**
     * {@link Map} of {@link PropertyGetter}s with the {@link Method} as the key.
     */
    private final Map<Method, PropertyGetter> propGetters = new HashMap<Method, PropertyGetter>();

    /**
     * {@link Map} of {@link PropertySetter}s with the {@link Method} as the key.
     */
    private final Map<Method, PropertySetter> propSetters = new HashMap<Method, PropertySetter>();

    /**
     * {@link Map} of {@link PropertyInfo}s with the property name as the key.
     */
    private final Map<String, PropertyInfo> propDefinitionsByName = new HashMap<String, PropertyInfo>();

    @SuppressWarnings("unchecked")
    public PropertyHandler(final Class<T> propertyInterface, final PropertyLoader loader)
        throws BadPropertyInfoException {
        /*
         * Implementation Note
         * 
         * This method (or any methods that it calls) cannot use the Prop#getProp4jProperties() method as this will potentially
         * cause a race condition. The reason for this is, that this method is invoked when creating a PropertyHandler which the
         * above method relies on.
         */
        if (propertyInterface == null) {
            throw new NullPointerException("Provided Property Interface is null");
        }

        if (propertyInterface.getInterfaces().length != 1 || propertyInterface.getInterfaces()[0] != PropertyBase.class) {
            throw new BadPropertyInfoException("Property Interface " + propertyInterface.getName() + " must extend "
                + PropertyBase.class.getSimpleName() + " directly");
        }

        this.propertyInterface = propertyInterface;
        this.loader = loader;
        this.proxy = (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { propertyInterface }, this);
        this.pSupport = new PropertyChangeSupport(proxy);

        // Analyse the property interface
        for (final Method m : propertyInterface.getDeclaredMethods()) {
            if (m.isAnnotationPresent(PropertyGetter.class)) {
                final PropertyGetter pi = m.getAnnotation(PropertyGetter.class);

                propGetters.put(m, pi);

                getOrCreatePropertyInfo(pi.name()).setGetter(pi);
            } else if (m.isAnnotationPresent(PropertySetter.class)) {
                final PropertySetter ps = m.getAnnotation(PropertySetter.class);

                propSetters.put(m, ps);

                getOrCreatePropertyInfo(ps.name()).setSetter(ps);
            } else {
                throw new BadPropertyInfoException("Method " + m.getName() + " has not got any property annotations defined");
            }
        }
    }

    public PropertyHandler(final Class<T> propertyInterface)
        throws BadPropertyInfoException {
        this(propertyInterface, Prop.getProp4jConfig().getLoader(propertyInterface));
    }

    private PropertyInfo getOrCreatePropertyInfo(final String propertyName) {
        if (!propDefinitionsByName.containsKey(propertyName)) {
            propDefinitionsByName.put(propertyName, new PropertyInfo());
        }

        return propDefinitionsByName.get(propertyName);
    }

    /**
     * @return The proxy for the Property Interface handled
     */
    public T getProxy() {
        return proxy;
    }

    @Override
    public Object invoke(final Object proxyObject, final Method method, final Object[] methodArgs) throws Throwable {
        /*
         * If the invoked method is declared in the PropertyBase, then we just invoke the method on this class
         */
        if (method.getDeclaringClass().equals(PropertyBase.class)) {
            return method.invoke(this, methodArgs);
        } else {
            /*
             * One of the property methods has been invoked.
             * 
             * Find out if it is a setter or a getter and call the appropriate method
             */
            if (propGetters.containsKey(method)) {
                final String propertyName = propGetters.get(method).name();
                if (loader != null) {
                    final Object propertyValue = loader.getProperty(propertyName);

                    if (propertyValue != null) {
                        return propertyValue;
                    }
                }

                return getDefaultValue(method.getReturnType(), propGetters.get(method));
            } else if (propSetters.containsKey(method)) {
                setProperty(propSetters.get(method).name(), methodArgs[0]);

                return null;
            } else {
                throw new IllegalStateException("Non property method invoked");
            }
        }
    }

    @Override
    public PropertyLoader getLoader() {
        return loader;
    }

    @Override
    public Object getProperty(final String propertyName)
        throws NullPointerException, IllegalArgumentException {
        if (propertyName == null) {
            throw new NullPointerException("Property name is null");
        }

        if (!propDefinitionsByName.containsKey(propertyName)) {
            throw new IllegalArgumentException("Property " + propertyName + " not defined in property interface " + propertyInterface.getName());
        }

        return loader.getProperty(propertyName);
    }

    private static <U> U getDefaultValue(final Class<U> asType,
        final PropertyGetter pi) throws BadPropertyInfoException {
        U defaultValue = null;
        try {
            defaultValue = convertValue(asType, pi.defaultValue());

        } catch (final NoSuchMethodException e) {
            throw new BadPropertyInfoException("Default value type (" + asType.getName() + ") for property " + pi.name()
                + " does not have a constructor taking a String");
        }

        return defaultValue;
    }

    @SuppressWarnings("unchecked")
    public static <U> U convertValue(final Class<U> asType, final String valueAsString) throws NoSuchMethodException {
        U defaultValue = null;

        try {
            if (valueAsString != null && !valueAsString.equals("")) {
                if (asType.getClass().isAssignableFrom(String.class)) {
                    defaultValue = (U) valueAsString;
                } else {
                    // Lets instantiate a new value
                    final Class[] parameterTypes = { String.class };

                    final Constructor<U> con = asType.getConstructor(parameterTypes);

                    final Object[] initargs = { valueAsString };
                    defaultValue = con.newInstance(initargs);
                }
            } else {
                // If the value is empty or null, create a new instance with no
                // parameters
                defaultValue = asType.newInstance();
            }
        } catch (final InstantiationException e) {
            throw new RuntimeException("Instantiation of " + asType.getName() + " failed", e);
        } catch (final IllegalAccessException e) {
            throw new RuntimeException("Instantiation of " + asType.getName() + " failed", e);
        } catch (final InvocationTargetException e) {
            throw new RuntimeException("Instantiation of " + asType.getName() + " failed", e);
        }

        return defaultValue;
    }

    @Override
    public void listProperties(final PrintStream out) {
        if (out == null) {
            throw new NullPointerException("PrintStream out is null");
        }

        for (final Entry<String, Object> prop : getProperties().entrySet()) {
            out.println(prop.getKey() + "=" + prop.getValue().toString());
        }
    }

    @Override
    public void setProperty(final String propName, final Object value)
        throws IllegalStateException, IllegalArgumentException {
        if (propName == null) {
            throw new NullPointerException("Property name is null");
        }

        if (!propDefinitionsByName.containsKey(propName)) {
            throw new IllegalArgumentException("Property " + propName
                + " not defined in property interface "
                + propertyInterface.getName());
        }

        if (loader != null) {
            if (loader.isMutable()) {
                final Object oldValue = getProperty(propName);

                loader.setProperty(propName, value);

                pSupport.firePropertyChange(propName, oldValue, value);
            } else {
                throw new IllegalStateException(
                    "The property loader is not mutable");
            }
        }
    }
    
    @Override
    public void setProperties(Properties newProperties) {
    	setProperties(newProperties);
    }
    
    @Override
    public void setProperties(Map<String, Object> newProperties) {
    	for (Entry<String, Object> property : newProperties.entrySet()) {
			setProperty(property.getKey(), property.getValue());
		}
    }

    @Override
    public Map<String, Object> getProperties() {
        final Map<String, Object> returnValue = new HashMap<String, Object>(
            propDefinitionsByName.size());

        if (loader != null) {
            for (final String propertyName : propDefinitionsByName.keySet()) {
                returnValue.put(propertyName, loader.getProperty(propertyName));
            }
        }

        return returnValue;
    }

    @Override
    public void addPropertyChangeListener(final PropertyChangeListener listener) {
        pSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(final String propertyName,
        final PropertyChangeListener listener) {
        pSupport.addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public void removePropertyChangeListener(
        final PropertyChangeListener listener) {
        pSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(final String propertyName,
        final PropertyChangeListener listener) {
        pSupport.removePropertyChangeListener(propertyName, listener);
    }
}
package net.toften.prop4j;

import net.toften.prop4j.config.Prop4jConfig;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.loaders.SystemPropertiesLoader;

/**
 * This is the Property Interface for prop4j.
 * <p>
 * The properties defined here are used by prop4j to determine how it will be configured.
 * <p>
 * All the prop4j properties are loaded from the {@link System#getProperties() system properties}
 * using the {@link SystemPropertiesLoader} {@link PropertyLoader} implementation.
 * 
 * @author thomaslarsen
 * @see Prop#init()
 */
public interface Prop4jProperties
    extends
        PropertyBase {
    /**
     * The property that provides a URL to the prop4j configuration file.
     */
    String PROP_PROPERTIES = "prop4j.configuration";

    /**
     * The property that defines which {@link net.toften.prop4j.config.Prop4jConfig config} class name that should be used if the
     * {@link #PROP_PROPERTIES} property is not provided.
     */
    String DEFAULT_PROP4J_CONFIG_CLASS = "prop4j.defaultconfigclass";

    /**
     * The property containing the Regex used to split the Property Interface hierarchy paths by the {@link Prop4jConfig}
     * implementations.
     */
    String PATH_SPLIT_REGEX = "prop4j.pathSplitRegex";

    /**
     * Get the prop4j config file URL.
     * 
     * @return prop4j config file URL
     */
    @PropertyGetter(name = PROP_PROPERTIES)
    String getConfigurationURL();

    /**
     * Get the default {@link net.toften.prop4j.config.Prop4jConfig config} class name.
     * <p>
     * This property defaults to the {@link net.toften.prop4j.config.ManualConfig} class.
     * 
     * @return default {@link net.toften.prop4j.config.Prop4jConfig config} class name
     */
    @PropertyGetter(name = DEFAULT_PROP4J_CONFIG_CLASS, defaultValue = "net.toften.prop4j.config.ManualConfig")
    String getDefaultConfigClassName();

    /**
     * Get the Regex used to split the Property Interface hierarchy paths.
     * <p>
     * This property defaults to "\\."
     * 
     * @return Regex used to split the Property Interface hierarchy paths
     */
    @PropertyGetter(name = PATH_SPLIT_REGEX, defaultValue = "\\.")
    String getPathSplitRegex();
}
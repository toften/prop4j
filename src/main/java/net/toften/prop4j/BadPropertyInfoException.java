package net.toften.prop4j;

public class BadPropertyInfoException
    extends
        Exception {

    public BadPropertyInfoException(final String string) {
        super(string);
    }
}
package net.toften.prop4j;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.Prop;
import net.toften.prop4j.Prop4jProperties;
import net.toften.prop4j.test.PropIFWithBadGetMethod;
import net.toften.prop4j.test.PropIFWithBadInheritance;

import org.junit.Before;
import org.junit.Test;


public class PropertyHandlerTest {
    @Before
    public void addSystemProperty() {
        System.getProperties().put(Prop4jProperties.PROP_PROPERTIES, "file:src/test/resources/prop4j.xml");
    }

    @Test(expected = BadPropertyInfoException.class)
    public void testPIWithBadInheritance() throws BadPropertyInfoException {
        Prop.getProp(PropIFWithBadInheritance.class);
    }

    @Test(expected = NullPointerException.class)
    public void testNullPI() throws BadPropertyInfoException {
        Prop.getProp(null);
    }

    @Test(expected = BadPropertyInfoException.class)
    public void PIWithInvalidMethod() throws BadPropertyInfoException {
        Prop.getProp(PropIFWithBadGetMethod.class);
    }
}
package net.toften.prop4j.loaders;

import static junit.framework.Assert.assertNotNull;
import net.toften.prop4j.Prop;
import net.toften.prop4j.config.ManualConfig;
import net.toften.prop4j.config.Prop4jConfig;

import org.junit.Before;
import org.junit.Test;

public abstract class GenericLoaderTest {
    private Prop4jConfig config = new ManualConfig();

    @Before
    public void setupProp4j() {
        Prop.init(config);
        
        config = Prop.getProp4jConfig();
        assertNotNull(config);
        
        assertNotNull(config.getDefaultLoader());
    }
    
    @Test
    public void testLoader() {
        config.addLoader(new MemoryPropertyLoader(), "net.toften");
    }
}

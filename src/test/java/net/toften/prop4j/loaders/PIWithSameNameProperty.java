package net.toften.prop4j.loaders;

import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.PropertyGetter;

public interface PIWithSameNameProperty extends PropertyBase {
    @PropertyGetter ( name = "server.name" )
    String getServer();
}

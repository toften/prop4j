package net.toften.prop4j.test;

import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.PropertyGetter;

public interface ValidPropertyInterface
    extends
        PropertyBase {

    public static final String SERVER_NAME_PROPERTY = "server.name";
    public static final String BLABLA_NAME_PROPERTY = "bla.bla";
    public static final String TIMEOUT_PROPERTY = "timeout";
    public static final String COUNT_PROPERTY = "count";
    
    public static final String DEFAULT_SERVER_NAME = "localhost";

    @PropertyGetter(name = SERVER_NAME_PROPERTY, defaultValue = DEFAULT_SERVER_NAME)
    public String getServerName();

    @PropertyGetter(name = BLABLA_NAME_PROPERTY)
    public Object getBlaBla();

    @PropertyGetter(name = TIMEOUT_PROPERTY, defaultValue = "1000")
    public Integer getTimeout();

    @PropertyGetter(name = COUNT_PROPERTY)
    public Integer count();
}

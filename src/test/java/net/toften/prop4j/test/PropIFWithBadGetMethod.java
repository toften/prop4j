package net.toften.prop4j.test;

import net.toften.prop4j.PropertyBase;

public interface PropIFWithBadGetMethod
    extends
        PropertyBase {
    String getProperty();
}

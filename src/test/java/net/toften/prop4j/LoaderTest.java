package net.toften.prop4j;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.Prop;
import net.toften.prop4j.Prop4jProperties;
import net.toften.prop4j.config.Prop4jConfig;
import net.toften.prop4j.loaders.MemoryPropertyLoader;
import net.toften.prop4j.test.ValidPropertyInterface;

import org.junit.Before;
import org.junit.Test;


public class LoaderTest {
    @Before
    public void enforceManualConfig() {
        System.getProperties().remove(Prop4jProperties.PROP_PROPERTIES);
        Prop.init();
    }

    @Test
    public void testCorrectConfigClass() throws ClassNotFoundException {
        final Prop4jProperties props = Prop.getProp4jProperties();

        final Class<? extends Prop4jConfig> configClass =
            (Class<? extends Prop4jConfig>) Class.forName(props.getDefaultConfigClassName());

        assertEquals(configClass, Prop.getProp4jConfig().getClass());
    }

    @Test
    public void testDefaultLoader() {
        assertNotNull(Prop.getProp4jConfig().getDefaultLoader());
        assertEquals(MemoryPropertyLoader.class, Prop.getProp4jConfig().getDefaultLoader().getClass());
    }

    @Test
    public void testCorrectLoaderIsLoaded() throws BadPropertyInfoException {
        final ValidPropertyInterface pi = Prop.getProp(ValidPropertyInterface.class);

        assertNotNull(pi.getLoader());

        assertEquals(Prop.getProp4jConfig().getDefaultLoader(), pi.getLoader());

        assertEquals(true, pi.getLoader().isMutable());
    }
}

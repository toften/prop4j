package net.toften.prop4j.config;

import static junit.framework.Assert.assertEquals;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.PropertyHandler;
import net.toften.prop4j.config.XMLConfig;
import net.toften.prop4j.test.ValidPropertyInterface;

import org.junit.Before;
import org.junit.Test;

public class XMLPropTest {
    private Prop4J prop;

    @Before
    public void loadXML() throws Exception {
        final JAXBContext context = JAXBContext.newInstance("net.toften.prop4j.config");

        final Unmarshaller um = context.createUnmarshaller();

        final InputStream propStream = getClass().getResourceAsStream("/prop4j.xml");

        prop = (Prop4J) um.unmarshal(propStream);
    }

    @Test
    public void setXMLProperties() throws BadPropertyInfoException, ClassNotFoundException, InstantiationException,
        IllegalAccessException {
        new XMLConfig(prop);
    }

    @Test
    public void testGetProperty() throws BadPropertyInfoException, ClassNotFoundException, InstantiationException,
        IllegalAccessException {
        final XMLConfig config = new XMLConfig(prop);

        final ValidPropertyInterface p =
            new PropertyHandler<ValidPropertyInterface>(ValidPropertyInterface.class,
            config.getLoader(ValidPropertyInterface.class)).getProxy();

        assertEquals("myhost", p.getServerName());
        assertEquals(new Integer(1000), p.getTimeout());
    }

    @Test
    public void testGetPropertyWithNoLoader() throws BadPropertyInfoException, ClassNotFoundException, InstantiationException,
        IllegalAccessException {
        final ValidPropertyInterface p =
            new PropertyHandler<ValidPropertyInterface>(ValidPropertyInterface.class, null).getProxy();

        assertEquals("localhost", p.getServerName());
        assertEquals(new Integer(1000), p.getTimeout());
    }
}
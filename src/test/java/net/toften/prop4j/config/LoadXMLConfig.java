package net.toften.prop4j.config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.Prop;
import net.toften.prop4j.Prop4jProperties;
import net.toften.prop4j.config.Prop4jConfig;
import net.toften.prop4j.config.XMLConfig;
import net.toften.prop4j.loaders.FilePropertyLoader;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.test.ValidPropertyInterface;

import org.junit.Before;
import org.junit.Test;


public class LoadXMLConfig {
    @Before
    public void addSystemProperty() {
        System.getProperties().put(Prop4jProperties.PROP_PROPERTIES, "file:src/test/resources/prop4j.xml");
        Prop.init();
    }

    @Test
    public void testLoadUsingSystemProperty() {
        final Prop4jConfig config = Prop.getProp4jConfig();

        assertEquals(XMLConfig.class, config.getClass());
    }

    @Test
    public void getLoader() throws BadPropertyInfoException {
        final ValidPropertyInterface prop = Prop.getProp(ValidPropertyInterface.class);
        assertNotNull(prop);

        final PropertyLoader loader = prop.getLoader();
        assertNotNull(loader);

        assertEquals(FilePropertyLoader.class, loader.getClass());
    }

    @Test
    public void getDefaultLoader() throws BadPropertyInfoException {
        final OtherPropIF prop = Prop.getProp(OtherPropIF.class);
        assertNotNull(prop);

        final PropertyLoader loader = prop.getLoader();
        assertNull(loader);
    }
}
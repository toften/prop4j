package net.toften.prop4j.config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.config.ManualConfig;
import net.toften.prop4j.loaders.MemoryPropertyLoader;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.test.ValidPropertyInterface;

import org.junit.Before;
import org.junit.Test;


public class Prop4JWrapperTest {
    private ManualConfig config;

    @Before
    public void setup() throws BadPropertyInfoException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        config = new ManualConfig();
    }

    @Test
    public void testGetWrapperWithNoAdapters() {
        assertEquals(config.getDefaultLoader(), config.getLoader(ValidPropertyInterface.class));
        assertEquals(1, config.getLoaderCount());
    }

    @Test
    public void checkRootAdapter() throws BadPropertyInfoException, ClassNotFoundException, InstantiationException,
        IllegalAccessException {
        assertEquals(1, config.getLoaderCount());

        assertNotNull(config.getDefaultLoader());
    }

    @Test
    public void testTwoPropIFs() throws BadPropertyInfoException, ClassNotFoundException, InstantiationException,
        IllegalAccessException {
        final PropertyLoader rootAdapter = config.getDefaultLoader();

        final PropertyLoader adapterPropIF = config.getLoader(ValidPropertyInterface.class);
        assertNotNull(adapterPropIF);
        assertEquals(rootAdapter, adapterPropIF);

        final PropertyLoader adapterOtherIF = config.getLoader(OtherPropIF.class);
        assertNotNull(adapterOtherIF);
        assertEquals(rootAdapter, adapterOtherIF);
        assertEquals(adapterPropIF, adapterOtherIF);
    }

    @Test
    public void addAdapterWithPath() throws BadPropertyInfoException, ClassNotFoundException, InstantiationException,
        IllegalAccessException {
        final PropertyLoader rootAdapter = config.getDefaultLoader();

        final PropertyLoader firstAdapter =
            config.addLoader(MemoryPropertyLoader.class.getName(), null, ValidPropertyInterface.class.getPackage().getName());
        assertNotSame(rootAdapter, firstAdapter);
        assertEquals(2, config.getLoaderCount());

        final PropertyLoader adapterPropIF = config.getLoader(ValidPropertyInterface.class);
        assertNotNull(adapterPropIF);
        assertEquals(firstAdapter, adapterPropIF);

        final PropertyLoader adapterOtherIF = config.getLoader(OtherPropIF.class);
        assertNotNull(adapterOtherIF);
        assertEquals(rootAdapter, adapterOtherIF);
        assertNotSame(adapterPropIF, adapterOtherIF);
    }

}
